const color0 = "rgb(232, 232, 232)";  // 灰
const color1 = "rgb(0, 0, 0)";        // 黒
const color2 = "rgb(192, 80, 77)";    // 赤
const color3 = "rgb(77, 80, 192)";    // 青
var intime = 100;  //  キャンバス最低更新間隔(ms)
var timer;

var sashiko_obj = function() {
  this.size = 4;

  this.cox = [
      [0,0,0],
      [0,0,0],
      [0,0,0]
  ];

  this.coy = [
      [0,0,0],
      [0,0,0],
      [0,0,0]
  ];

  this.com = [
      [0],
      [0,0],
      [0,0,0],
      [0,0],
      [0]
  ];
  this.con = [
      [0],
      [0,0],
      [0,0,0],
      [0,0],
      [0]
  ];

// cox―横線  coy｜縦線  com＼斜線  con／斜線
//  ①②③     ①④⑦     ④⑦⑨     ①③⑥
//  ④⑤⑥     ②⑤⑧     ②⑤⑧     ②⑤⑧
//  ⑦⑧⑨     ③⑥⑨     ①③⑥     ④⑦⑨

  this.resize = function(x) {
    this.size = x;
    this.cox = new Array(x - 1);
    for (var i = 0; i < x - 1; i++) {
      this.cox[i] = new Array(x - 1).fill(0);
    }
    this.coy = new Array(x - 1);
    for (var i = 0; i < x - 1; i++) {
      this.coy[i] = new Array(x - 1).fill(0);
    }
    this.com = new Array(x * 2 - 3);
    for (var i = 0; i < x - 1; i++) {
      this.com[i] = new Array(i + 1).fill(0);
    }
    for (var i = x - 1; i < x * 2 - 3; i++) {
      this.com[i] = new Array(x * 2 - 3 - i).fill(0);
    }
    this.con = new Array(x * 2 - 3);
    for (var i = 0; i < x - 1; i++) {
      this.con[i] = new Array(i + 1).fill(0);
    }
    for (var i = x - 1; i < x * 2 - 3; i++) {
      this.con[i] = new Array(x * 2 - 3 - i).fill(0);
    }
  }
};

var sashiko_canvas = function() {
  const spacing = 40;
  const radius = 4;
  const padding = 10;
  const standardLineWidth = 3;

  this.isPeripheralDisplay = 1;

  this.bgDraw = function(ctx) {
    ctx.lineWidth = standardLineWidth;
    ctx.clearRect(0, 0, canvas.width, canvas.height);

    for (var i = 0; i < sashiko.cox.length; i++) {
      for (var j = 0; j < sashiko.cox[i].length; j++) {
        this.line5(ctx, j, i, 1, 0, color0);
      }
    }
    for (var j = 0; j < sashiko.cox[0].length; j++) {
      this.line5(ctx, j, sashiko.cox.length, 1, 0, color0);
    }
    for (var i = 0; i < sashiko.coy.length; i++) {
      for (var j = 0; j < sashiko.coy[i].length; j++) {
        this.line5(ctx, i, j, 0, 1, color0);
      }
    }
    for (var j = 0; j < sashiko.coy[0].length; j++) {
      this.line5(ctx, sashiko.coy.length, j, 0, 1, color0);
    }
    for (var i = 0; i < sashiko.com.length; i++) {
      for (var j = 0; j < sashiko.com[i].length; j++) {
        if (i < sashiko.size - 1) {
          this.line5(ctx, j, sashiko.size - 2 - i + j, 1, 1, color0);
        } else {
          this.line5(ctx, i - sashiko.size + 2 + j, j, 1, 1, color0);
        }
      }
    }
    for (var i = 0; i < sashiko.con.length; i++) {
      for (var j = 0; j < sashiko.con[i].length; j++) {
        if (i < sashiko.size - 1) {
          this.line5(ctx, j, i - j + 1, 1, -1, color0);
        } else {
          this.line5(ctx, j + i - sashiko.size + 2, sashiko.size - 1 - j, 1, -1, color0);
        }
      }
    }

    var endi = sashiko.size;
    var endj = sashiko.size;
    if (this.isPeripheralDisplay) {
      endi = sashiko.size * 3 - 2;
      endj = sashiko.size * 3 - 2;
    }
    for (var i = 0; i < endi; i++) {
      for (var j = 0; j < endj; j++) {
        ctx.beginPath();
        ctx.fillStyle = color2;
        ctx.arc(padding + spacing * j, padding + spacing * i , radius, 0, Math.PI * 2, false);
        ctx.fill();
      }
    }
  };

  this.entityDraw = function(ctx) {
    for (var i = 0; i < sashiko.cox.length; i++) {
      for (var j = 0; j < sashiko.cox[i].length; j++) {
        if (sashiko.cox[i][j] == 1) {
          this.line9(ctx, j, i, 1, 0);
        }
      }
    }
    for (var j = 0; j < sashiko.cox[0].length; j++) {
      if (sashiko.cox[0][j] == 1) {
        this.line9(ctx, j, sashiko.cox.length, 1, 0);
      }
    }
    for (var i = 0; i < sashiko.coy.length; i++) {
      for (var j = 0; j < sashiko.coy[i].length; j++) {
        if (sashiko.coy[i][j] == 1) {
          this.line9(ctx, i, j, 0, 1);
        }
      }
    }
    for (var j = 0; j < sashiko.coy[0].length; j++) {
      if (sashiko.coy[0][j] == 1) {
        this.line9(ctx, sashiko.coy.length, j, 0, 1);
      }
    }
    for (var i = 0; i < sashiko.com.length; i++) {
      for (var j = 0; j < sashiko.com[i].length; j++) {
        if (sashiko.com[i][j] == 1) {
          if (i < sashiko.size - 1) {
            this.line9(ctx, j, sashiko.size - 2 - i + j, 1, 1);
          } else {
            this.line9(ctx, i - sashiko.size + 2 + j, j, 1, 1);
          }
        }
      }
    }
    for (var i = 0; i < sashiko.con.length; i++) {
      for (var j = 0; j < sashiko.con[i].length; j++) {
        if (sashiko.con[i][j] == 1) {
          if (i < sashiko.size - 1) {
            this.line9(ctx, j, i - j + 1, 1, -1);
          } else {
            this.line9(ctx, j + i - sashiko.size + 2, sashiko.size - 1 - j, 1, -1);
          }
        }
      }
    }
  };

  this.line5 = function(context, startx, starty, dx, dy, color) {
    if (this.isPeripheralDisplay) {
      this.line(context, startx + (sashiko.size - 1), starty + (sashiko.size - 1), dx, dy, color);
    } else {
      this.line(context, startx, starty, dx, dy, color);
    }
  };

  this.line9 = function(context, startx, starty, dx, dy, color) {
    if (this.isPeripheralDisplay) {
      for (var i = 0; i < 3; i++) {
        for (var j = 0; j < 3; j++) {
          this.line(context, startx + i * (sashiko.size - 1), starty + j * (sashiko.size - 1), dx, dy, color);
        }
      }
    } else {
      this.line(context, startx, starty, dx, dy, color);
    }
  };

  this.line = function(context, startx, starty, dx, dy, color) {
    context.beginPath();
    if (color) {
      context.strokeStyle = color;
    } else {
      context.strokeStyle = color1;
    }
    context.moveTo(padding + spacing * startx, padding + spacing * starty);
    context.lineTo(padding + spacing * (startx + dx), padding + spacing * (starty + dy));
    context.closePath();
    context.stroke();
  };

  this.mouseHit = function(e) {
    var rect = e.target.getBoundingClientRect();
    var x = e.clientX - rect.left;
    var y = e.clientY - rect.top;
    var offset = padding;
    if (this.isPeripheralDisplay) {
      offset += (sashiko.size - 1) * spacing;
    }

    for (var j = 0; j < sashiko.cox.length + 1; j++) {
      if (((j - (y - offset) / spacing) < 0.2) && (j - (y - offset) / spacing) > -0.2) {
        var aj = j;
        var ai = Math.floor((x - offset) / spacing);
        if (ai >= 0 && ai < sashiko.size - 1) {
          return ["cox", aj, ai];
        }
      }
    }

    for (var i = 0; i < sashiko.coy.length + 1; i++) {
      if (((i - (x - offset) / spacing) < 0.2) && (i - (x - offset) / spacing) > -0.2) {
        var ai = i;
        var aj = Math.floor((y - offset) / spacing);
        if (aj >= 0 && aj < sashiko.size - 1) {
          return ["coy", aj, ai];
        }
      }
    }

    for (var i = 0; i < sashiko.com.length; i++) {
      if (((i - Math.abs(x - y) / spacing) < 0.2) && (i - Math.abs(x - y) / spacing) > -0.2) {
        var ai = sashiko.size - 2 + i;
        if (x < y) {
          ai = sashiko.size - 2 - i;
        }
        var aj = Math.floor(Math.min(x, y) / spacing);
        if (this.isPeripheralDisplay) {
          aj -= sashiko.size - 1;
        }
        if (ai < 0 || aj < 0) { break;}
        if (sashiko.com.length > ai && sashiko.com[ai].length > aj) {
          return ["com", aj, ai];
        }
      }
    }

    for (var i = 0; i < sashiko.con.length; i++) {
      if ((((x + y) / 2 - offset) / spacing - (i + 1) / 2 < 0.2) && (((x + y) / 2 - offset) / spacing - (i + 1) / 2 > -0.2)) {
        var ai = i;
        var aj = Math.floor((x - offset) / spacing);
        if (ai > sashiko.size - 2) {
          aj = Math.floor((x - offset) / spacing) - ai + sashiko.size - 2;
        }
        if (ai < 0 || aj < 0) { break;}
        if (sashiko.con.length > ai && sashiko.con[ai].length > aj) {
          return ["con", aj, ai];
        }
      }
    }
  }
};

var sashiko = new sashiko_obj();
var sashikoCanvas = new sashiko_canvas();

onload = function() {
  canvas = document.getElementById("canvas1");
  if (!canvas || !canvas.getContext) {
    return false;
  }
  canvas.addEventListener("mousemove", onMouseMove, false);
  canvas.addEventListener("click", onClick, false);
  initHtml();
  draw();
};

function throttle(targetFunc, time) {
  var _time = time || intime;
  clearTimeout(this.timer);
  this.timer = setTimeout(function() {
    targetFunc();
  }, _time);
}

function onMouseMove(e) {
  draw(e);
}

function onClick(e) {
  var ret = sashikoCanvas.mouseHit(e);
  if (ret != null) {
    var aj = ret[1];
    var ai = ret[2];
    if (ret[0] == "cox") {
    if (aj == sashiko.cox.length) { aj = 0;}
      if (sashiko.cox[aj][ai] == 1) {
        sashiko.cox[aj][ai] = 0;
      } else {
        sashiko.cox[aj][ai] = 1;
      }
    } else if (ret[0] == "coy") {
      if (ai == sashiko.coy.length) { ai = 0;}
      if (sashiko.coy[ai][aj] == 1) {
        sashiko.coy[ai][aj] = 0;
      } else {
        sashiko.coy[ai][aj] = 1;
      }
    } else if (ret[0] == "com") {
      if (sashiko.com[ai][aj] == 1) {
        sashiko.com[ai][aj] = 0;
      } else {
        sashiko.com[ai][aj] = 1;
      }
    } else if (ret[0] == "con") {
      if (sashiko.con[ai][aj] == 1) {
        sashiko.con[ai][aj] = 0;
      } else {
        sashiko.con[ai][aj] = 1;
      }
    }
  }
  draw(e);
}

function draw(e) {
  throttle(function() {
    var ctx = canvas.getContext("2d");
    sashikoCanvas.bgDraw(ctx);
    sashikoCanvas.entityDraw(ctx);

    if (!e) {return;}
    var ret = sashikoCanvas.mouseHit(e);
    if (ret != null) {
      var aj = ret[1];
      var ai = ret[2];
      if (ret[0] == "cox") {
        var aj2 = aj;
        if (aj == sashiko.cox.length) {  //  下端は上端と同じ表示
          aj = 0;
        }
        if (sashiko.cox[aj][ai] == 1) {
          sashikoCanvas.line5(ctx, ai, aj2, 1, 0, color2);
        } else {
          sashikoCanvas.line5(ctx, ai, aj2, 1, 0, color3);
        }
      } else if (ret[0] == "coy") {
        var ai2 = ai;
        if (ai == sashiko.coy.length) {
          ai = 0;
        }
        if (sashiko.coy[ai][aj] == 1) {
          sashikoCanvas.line5(ctx, ai2, aj, 0, 1, color2);
        } else {
          sashikoCanvas.line5(ctx, ai2, aj, 0, 1, color3);
        }
      } else if (ret[0] == "com") {
        if (sashiko.com[ai][aj] == 1) {
          if (ai < sashiko.size - 1) {
            sashikoCanvas.line5(ctx, aj, sashiko.size - 2 - ai + aj, 1, 1, color2);
          } else {
            sashikoCanvas.line5(ctx, ai - sashiko.size + 2 + aj, aj, 1, 1, color2);
          }
        } else {
          if (ai < sashiko.size - 1) {
            sashikoCanvas.line5(ctx, aj, sashiko.size - 2 - ai + aj, 1, 1, color3);
          } else {
            sashikoCanvas.line5(ctx, ai - sashiko.size + 2 + aj, aj, 1, 1, color3);
          }
        }
      } else if (ret[0] == "con") {
        if (sashiko.con[ai][aj] == 1) {
          if (ai < sashiko.size - 1) {
            sashikoCanvas.line5(ctx, aj, ai - aj + 1, 1, -1, color2);
          } else {
            sashikoCanvas.line5(ctx, aj + ai - sashiko.size + 2, sashiko.size - 1 - aj, 1, -1, color2);
          }
        } else {
          if (ai < sashiko.size - 1) {
            sashikoCanvas.line5(ctx, aj, ai - aj + 1, 1, -1, color3);
          } else {
            sashikoCanvas.line5(ctx, aj + ai - sashiko.size + 2, sashiko.size - 1 - aj, 1, -1, color3);
          }
        }
      }
    }
  }, intime);
}

function initHtml() {
  var x = sashiko.size;
  document.getElementById("canvastitle").innerHTML = "基本格子 " + x + "x" + x;
}

function canvasChange() {
  if (document.getElementById("change1").style.display == "block") {
    document.getElementById("change1").style.display = "none";
  } else {
    document.getElementById("change1").style.display = "block";
  }
}

function canvasResize() {
  document.getElementById("change1").style.display = "none";
  var x = document.getElementById("canvassize").selectedIndex + 3;
  document.getElementById("canvastitle").innerHTML = "基本格子 " + x + "x" + x;
  sashiko.resize(x);
  draw();
}

function displayPeripheral() {
  if (sashikoCanvas.isPeripheralDisplay) {
    sashikoCanvas.isPeripheralDisplay = 0;
    document.getElementById("displayperipheral").innerHTML = "周囲を表示する";
  } else {
    sashikoCanvas.isPeripheralDisplay = 1;
    document.getElementById("displayperipheral").innerHTML = "周囲を表示しない";
  }
  draw();
}

function canvasClear() {
  if (confirm("本当にクリアしますか？")) {
    sashiko.resize(sashiko.size);
    draw();
  }
}

function evaluation() {
  var contLength1 = [];
  var contLength2 = [];
  for (var i = 0; i < sashiko.cox.length; i++) {
    var tempArray = sashiko.cox[i].concat(sashiko.cox[i]);  //  2つつける
    continuous(tempArray, contLength1);
  }
  for (var i = 0; i < sashiko.coy.length; i++) {
    var tempArray = sashiko.coy[i].concat(sashiko.coy[i]);
    continuous(tempArray, contLength1);
  }
  var maxm = Math.floor(sashiko.com.length / 2);
  for (var i = 0; i < maxm; i++) {
    var tempArray = sashiko.com[i].concat(sashiko.com[i + maxm + 1]);  //  連続につなげる
    var tempArray2 = tempArray.concat(tempArray);
    continuous(tempArray2, contLength2);
  }
  var tempArray2 = sashiko.com[maxm].concat(sashiko.com[maxm]);
  continuous(tempArray2, contLength2);

  var maxn = Math.floor(sashiko.con.length / 2);
  for (var i = 0; i < maxn; i++) {
    var tempArray = sashiko.con[i].concat(sashiko.con[i + maxn + 1]);
    var tempArray2 = tempArray.concat(tempArray);
    continuous(tempArray2, contLength2);
  }
  tempArray2 = sashiko.con[maxn].concat(sashiko.con[maxn]);
  continuous(tempArray2, contLength2);

  var maxContLength1 = Math.max.apply(null, contLength1);
  var maxContLength2 = Math.max.apply(null, contLength2);
  if (maxContLength1 >= sashiko.size) {
    maxContLength1 = "∞";
  }
  if (maxContLength2 >= sashiko.size) {
    maxContLength2 = "∞";
  }
  alert("縦横に連続" + maxContLength1 + "マス, 斜めに連続" + maxContLength2 + "マス");

 function continuous(a, c) {
   var j = 0;
   var cont = 0;
   while(j < a.length) {
     if (a[j] == 1) {
       cont++;
     } else {
       c.push(cont);
       cont = 0;
     }
     j++;
   }
   c.push(cont);
  }
}

function selectTrad() {
  if (document.getElementById("change2").style.display == "block") {
    document.getElementById("change2").style.display = "none";
  } else {
    document.getElementById("change2").style.display = "block";
  }
}

function loadTrad() {
  // TODO
}